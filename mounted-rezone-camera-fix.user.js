// ==UserScript==
// @name        fix mounted rezone camera
// @version     1.0.2
// @description fixes the bug where your camera speed is wrong after rezoning while mounted
// @match       http*://play.isleward.com/*
// @match       http*://ptr.isleward.com/*
// @namespace   Isleward.Addon
// ==/UserScript==

// See here: https://gitlab.com/Isleward-Modding/addons/addonmanager#addon-development
(a=>{let p=_=>{let m=globalThis[typeof ADDON_MANAGER=='undefined'?'addons':ADDON_MANAGER];m?m.register(a):setTimeout(p,200)};p()})({
	id: typeof ADDON_ID === 'undefined' ? 'mounted-rezone-camera-fix' : ADDON_ID,
	name: 'Fix Mounted Rezone Camera',
    init: () => {
        console.log('[mounted rezone camera] fixed');

        window.addons.events.on('onRezone', () => {
            window.addons.events.emit('onMoveSpeedChange', 0);
        });
    }
});
